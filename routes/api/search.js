var router = require('express').Router();
var mongoose = require('mongoose');
var Album = mongoose.model('Album');
var Articles = mongoose.model('Article');
var File = mongoose.model('File');
var InfoDinas = mongoose.model('InfoDinas');
var Pengumuman = mongoose.model('PengumumanDinas');
var User = mongoose.model('User');
var auth = require('../auth');


router.get('/:search', auth.optional, function(req, res, next) {
    var query = {};
    var limit = 20;
    var offset = 0;
  
    if(typeof req.query.limit !== 'undefined'){
      limit = req.query.limit;
    }
  
    if(typeof req.query.offset !== 'undefined'){
      offset = req.query.offset;
    }
  
    Promise.all([
        Album.find({ $text: { $search:req.params.search }})
          .sort({createdAt: 'desc'})
          .populate('author')
          .populate('category')
          .populate('gallery')
          .exec(),
        Articles.find({ $text: { $search:req.params.search }})
          .sort({createdAt: 'desc'})
          .populate('author')
          .populate('category')
          .exec(),
        File.find({ $text: { $search:req.params.search }})
          .sort({createdAt: 'desc'})
          .populate('author')
          .populate('category')
          .exec(),
        Pengumuman.find({ $text: { $search:req.params.search }})
          .sort({createdAt: 'desc'})
          .populate('author')
          .populate('category')
          .exec(),
        InfoDinas.find({ $text: { $search:req.params.search }})
          .sort({createdAt: 'desc'})
          .populate('author')
          .exec(),
        req.payload ? User.findById(req.payload.id) : null,
      ]).then(function(results){
        console.log(results);
        var data = {},
            count = 0;

        if(!results[0].length < 1){
            data.album = results[0].map(function(album){
                return album.toJSONFor();
            })

            count += results[0].length
        }
    
        if(!results[1].length < 1){
            data.article = results[1].map(function(article){
                return article.toJSONFor();
            })
            count += results[1].length
        }

        if(!results[2].length < 1){
            data.file=results[2].map(function(file){
                return file.toJSONFor();
            })

            count += results[2].length
        }

        if(!results[3].length < 1){
            data.pengumuman = results[4].map(function(pengumuman){
                return pengumuman.toJSONFor();
            })

            count += results[4].length
        }

        if(!results[4].length < 1){
            data.infoDinas = results[6].map(function(info){
                return info.toJSONFor();
            }),
            
            count += results[6].length
        }
        
        return res.json({
          success: true,
          message: count > 0 ? 'Found' : 'Not Found',
          data: data,
          count: count
        });
    });
});

module.exports = router;