var router = require('express').Router();
var mongoose = require('mongoose');
var Category = mongoose.model('Category');
var auth = require('../auth');
var User = mongoose.model('User');

// return a list of categories
router.get('/', function(req, res, next) {
  Category.find().then(function(categories){
    return res.json({success: true, data: categories});
  }).catch(next);
});

//Category by id
router.get('/:id', function(req, res){
  Category.findById(req.params.Id).then(function(category){
    
    return res.json({success: true, data: category})

  })
});

// Create a new category
router.post('/', auth.required, function(req, res, next) {
  User.findById(req.payload.id).then(function(user){
    if (!user) { return res.sendStatus(401); }

    var category = new Category(req.body);

    category.author = user;

    return category.save().then(function(){
      return res.json({success: true, message: "Berhasil menyimpan kategori"});
    });
  }).catch(next);
});

// update category
router.put('/:id', auth.required, function(req, res, next) {
  User.findById(req.payload.id).then(function(user){
      let updCategory = {};
      if(typeof req.body.name !== 'undefined'){
        updCategory.name = req.body.name;
      }
      updCategory.updatedBy = user;
      
      Category.findByIdAndUpdate(
        req.params.id,
        updCategory,
        {new:true}
        ).exec().then(function(category){
          if (!category) return res.status(404).json({success:false, message:'Category Not Found'});
          return res.json({success:true, message: "Berhasil mengupdate kategori"});
      })
  });
});

// delete article
router.delete('/:id', auth.required, function(req, res, next) {
  User.findById(req.payload.id).then(function(user){
    if (!user) { return res.sendStatus(401); }

    Category.findByIdAndRemove(req.params.id).exec().then(function(category){
      return res.status(204).json({success:true, message: "Behasil menghapus kategori"});
    })
  }).catch(next);
});

module.exports = router;
