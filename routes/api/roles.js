var router = require('express').Router();
var mongoose = require('mongoose');
var Role = mongoose.model('Role');
var auth = require('../auth');
var User = mongoose.model('User');

// return a list of roles
router.get('/', function(req, res, next) {
  Role.find().then(function(role){
    return res.json({success:true, data:role});
  }).catch(next);
});

// get role by id
router.get('/:id', function(req, res){ 
  Role.findById(req.params.id).then(function(role){
     return res.json({success:true, data:role})
  })
});

router.post('/', auth.required, function(req, res, next) {
  User.findById(req.payload.id).then(function(user){
    if (!user) { return res.sendStatus(401); }

    var role = new Role();
    
    role.author = user;
    role.name = req.body.name;
    role._id = req.body.id;
    
    return role.save().then(function(){
      return res.json({success:true, message: "berhasil menambah role baru"});
    });
  }).catch(next);
});

// update role
router.put('/:id', auth.required, function(req, res, next) {
  User.findById(req.payload.id).then(function(user){
      let updRole = {};
      if(typeof req.body.name !== 'undefined'){
        updRole.name = req.body.name;
      }
      if(typeof req.body.id !== 'undefined'){
        updRole.name = req.body._id;
      }
      updRole.updatedBy = user;
      
      Role.findByIdAndUpdate(
        req.params.id,
        updRole,
        {new:true}
        ).exec().then(function(role){
          if (!role) return res.status(404).json({success:false, message:'Role Not Found'});
          return res.status(200).json({success:true, message: "role berhasil diupdate"});
        }).catch(function(err){
          console.log(err);
        })
  });
});

// delete role
router.delete('/:id', auth.required, function(req, res, next) {
  User.findById(req.payload.id).then(function(user){
    if (!user) { return res.sendStatus(401); }

    Role.findByIdAndRemove(req.params.id).exec().then(function(role){
      return res.status(204).json({success:true, message: "role berhasil dihapus"});
    })
  }).catch(next);
});

module.exports = router;
