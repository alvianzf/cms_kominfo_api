var router = require('express').Router();
var mongoose = require('mongoose');
var Pengumuman = mongoose.model('PengumumanDinas');
var Category = mongoose.model('Category');
var Comment = mongoose.model('Comment');
var User = mongoose.model('User');
var auth = require('../auth');

const upload = require('../../config/uploads').uploads('gallery');

// Preloads pengumuman objects on routes with ':pengumuman'
router.param('pengumuman', function(req, res, next, slug) {
    Pengumuman.findOne({ slug: slug})
    .populate('author')
    .then(function (pengumuman) {
      if (!pengumuman) { return res.status(404).json({success:false, message:'Not Found', }); }

      req.pengumuman = pengumuman;

      return next();
    }).catch(next);
});

router.param('comment', function(req, res, next, id) {
  Comment.findById(id).then(function(comment){
    if(!comment) { return res.status(404).json({success:false, message:'Not Found'}); }

    req.comment = comment;

    return next();
  }).catch(next);
});

router.get('/', auth.optional, function(req, res, next) {
  var query = {};
  var limit = 20;
  var offset = 0;
  console.log(req.params.opd);
  if(typeof req.query.limit !== 'undefined'){
    limit = req.query.limit;
  }

  if(typeof req.query.offset !== 'undefined'){
    offset = req.query.offset;
  }

  if( typeof req.query.category !== 'undefined' ){
    query.category = {"$in" : [req.query.category]};
  }

  Promise.all([
    req.query.author ? User.findOne({username: req.query.author}) : null,
    req.query.favorited ? User.findOne({username: req.query.favorited}) : null
  ]).then(function(results){
    var author = results[0];
    var favoriter = results[1];

    if(author){
      query.author = author._id;
    }

    if(favoriter){
      query._id = {$in: favoriter.favorites};
    } else if(req.query.favorited){
      query._id = {$in: []};
    }

    return Promise.all([
    Pengumuman.find(query)
        .limit(Number(limit))
        .skip(Number(offset))
        .sort({createdAt: 'desc'})
        .populate('author')
        .populate('category')
        .exec(),
      Pengumuman.count(query).exec(),
      req.payload ? User.findById(req.payload.id) : null,
    ]).then(function(results){
      var pengumumans = results[0];
      var pengumumansCount = results[1];
      var user = results[2];

      return res.json({
        success: true,
        data: pengumumans.map(function(pengumuman){
          return pengumuman;

        }),
        count: pengumumansCount
      });
    });
  }).catch(next);
});

router.get('/opd/:opd', auth.optional, function(req, res, next) {
  var query = {};
  var limit = 20;
  var offset = 0;
  if(typeof req.query.limit !== 'undefined'){
    limit = req.query.limit;
  }

  if(typeof req.query.offset !== 'undefined'){
    offset = req.query.offset;
  }

  if( typeof req.params.opd !== 'undefined' ){
    query.opd = req.params.opd;
  }
  
  Promise.all([
    Pengumuman.find(query)
      .limit(Number(limit))
      .skip(Number(offset))
      .sort({createdAt: 'desc'})
      .populate('author')
      .populate('category')
      .exec(),
    Pengumuman.count(query).exec(),
    req.payload ? User.findById(req.payload.id) : null,
  ]).then(function(results){
    var pengumumans = results[0];
    var pengumumansCount = results[1];

    return res.json({
      success: true,
      data: pengumumans.map(function(pengumuman){
        return pengumuman;

      }),
      count: pengumumansCount
    });
  }).catch(next);
});

router.get('/category/:category', auth.optional, function(req, res, next) {
  var query = {};
  var limit = 20;
  var offset = 0;
  var category = "";
  if(typeof req.query.limit !== 'undefined'){
    limit = req.query.limit;
  }

  if(typeof req.query.offset !== 'undefined'){
    offset = req.query.offset;
  }

  if( typeof req.params.category !== 'undefined' ){
    Category.findOne({name:req.params.category},{'_id':1})
    .exec().then(function(result){
      query.category = result._id;
    })
  }
  
  Promise.all([
    Pengumuman.find(query)
      .limit(Number(limit))
      .skip(Number(offset))
      .sort({createdAt: 'desc'})
      .populate('author')
      .populate('category')
      .exec(),
    Pengumuman.count(query).exec(),
    req.payload ? User.findById(req.payload.id) : null,
  ]).then(function(results){
    var pengumumans = results[0];
    var pengumumansCount = results[1];

    return res.json({
      success: true,
      data: pengumumans.map(function(pengumuman){
        return pengumuman;

      }),
      count: pengumumansCount
    });
  }).catch(next);
});

router.post('/', [auth.required, upload.single('thumbnail')], function(req, res, next) {
  User.findById(req.payload.id).then(function(user){
    if (!user) { return res.sendStatus(401); }

    var pengumuman = new Pengumuman(req.body);

    if(req.file){
      pengumuman.thumbnail = req.file.path;
    }

    pengumuman.author = user;

    return pengumuman.save().then(function(){
      return res.json({success:true, message: "pengumuman berhasil disimpan"});
    });
  }).catch(next);
});

// return a pengumuman
router.get('/:pengumuman', auth.optional, function(req, res, next) {
  Promise.all([
    req.payload ? User.findById(req.payload.id) : null,
    req.pengumuman.populate('author').execPopulate()
  ]).then(function(results){
    var user = results[0];
    req.pengumuman.updateViewCount().then(function(pengumuman){
      return res.json({success:true, data: pengumuman});
    });
  }).catch(next);
});

// update pengumuman
router.put('/:pengumuman', [auth.required, upload.single('thumbnail')], function(req, res, next) {
  User.findById(req.payload.id).then(function(user){
    if(req.pengumuman.author._id.toString() === req.payload.id.toString()){
      if(typeof req.body.title !== 'undefined'){
        req.pengumuman.title = req.body.title;
      }

      if(typeof req.body.description !== 'undefined'){
        req.pengumuman.description = req.body.description;
      }

      if(typeof req.file !== 'undefined'){
        req.pengumuman.thumbnail = req.file.path;
      }

      if(typeof req.body.body !== 'undefined'){
        req.pengumuman.body = req.body.body;
      }

      if(typeof req.body.category !== 'undefined'){
        req.pengumuman.category = req.body.category
      }

      req.pengumuman.save().then(function(pengumuman){
        return res.json({success:true, message: "berhasil mengupdate pengumuman"});
      }).catch(next);
    } else {
      return res.status(403).json({success:true, message:"Forbidden"});
    }
  });
});

// delete pengumuman
router.delete('/:pengumuman', auth.required, function(req, res, next) {
  User.findById(req.payload.id).then(function(user){
    console.log(user);
    if (!user) { return res.sendStatus(401); }
    return req.pengumuman.remove().then(function(){
      return res.status(204).json({success:true, message:"pengumuman deleted"});
    }).catch(next);
  }).catch(next);
});

// Favorite an pengumuman
router.post('/:pengumuman/favorite', auth.required, function(req, res, next) {
  var pengumumanId = req.pengumuman._id;
  User.findById(req.payload.id).then(function(user){
    if (!user) { return res.sendStatus(401); }

    return user.favorite(pengumumanId).then(function(){
      return req.pengumuman.updateFavoriteCount().then(function(pengumuman){
        return res.json({success:true, message:"Berhasil disimpan"});
      });
    });
  }).catch(next);
});

// Unfavorite an pengumuman
router.delete('/:pengumuman/favorite', auth.required, function(req, res, next) {
  var pengumumanId = req.pengumuman._id;
  
  User.findById(req.payload.id).then(function (user){
    if (!user) { return res.sendStatus(401); }

    return user.unfavorite(pengumumanId).then(function(){
      return req.pengumuman.updateFavoriteCount().then(function(pengumuman){
        return res.json({success:true, message:"Berhasil dihapus"});
      });
    });
  }).catch(next);
});

// return an pengumuman's comments
router.get('/:pengumuman/comments', function(req, res, next){
  Promise.resolve(req.payload ? User.findById(req.payload.id) : null).then(function(user){
    return req.pengumuman.populate({
      path: 'comments',
      populate: {
        path: 'author'
      },
      options: {
        sort: {
          createdAt: 'desc'
        }
      }
    }).execPopulate().then(function(pengumuman) {
      return res.json({success:true, data: req.pengumuman.comments.map(function(comment){
        return comment.toJSONFor(user);
      })});
    });
  }).catch(next);
});

// create a new comment
router.post('/:pengumuman/comments', auth.required, function(req, res, next) {
  User.findById(req.payload.id).then(function(user){
    if(!user){ return res.sendStatus(401); }

    var comment = new Comment(req.body);
    comment.pengumuman = req.pengumuman;
    comment.author = user;

    return comment.save().then(function(){
      req.pengumuman.comments = req.pengumuman.comments.concat([comment]);

      return req.pengumuman.save().then(function(pengumuman) {
        res.json({success:true, message: "komen Berhasil"});
      });
    });
  }).catch(next);
});

router.delete('/:pengumuman/comments/:comment', auth.required, function(req, res, next) {
  if(req.comment.author.toString() === req.payload.id.toString()){
    req.pengumuman.comments.remove(req.comment._id);
    req.pengumuman.save()
      .then(Comment.find({_id: req.comment._id}).remove().exec())
      .then(function(){
        res.status(204).json({success:true, message:"Comment Deleted"})
      });
  } else {
    res.status(403).json({success:false, message:"Forbidden"});
  }
});

module.exports = router;
