var router = require('express').Router();
var mongoose = require('mongoose');
var User = mongoose.model('User');
var auth = require('../auth');

// Preload user profile on routes with ':username'
router.param('idpeg', function(req, res, next, idpeg){
  User.findOne({idPeg: idpeg}).then(function(user){
    if (!user) { return res.json({success: false, message: "user not found"}); }

    req.profile = user;

    return next();
  }).catch(next);
});

router.get('/:idpeg', auth.optional, function(req, res, next){
  if(req.payload){
    User.findById(req.payload.id).then(function(user){
      if(!user){ return res.status(403).json({success:false, data: req.profile.toProfileJSONFor(false)}); }

      return res.json({success:true, data: req.profile.toProfileJSONFor(user)});
    });
  } else {
    return res.status(403).json({success:false, data: req.profile.toProfileJSONFor(false)});
  }
});

module.exports = router;
