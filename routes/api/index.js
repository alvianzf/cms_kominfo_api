var router = require('express').Router();
var mongoose = require('mongoose');
var Role = mongoose.model('Role');
var seeder = require('mongoose-seeder');

router.use('/', require('./users'));
router.use('/role', require('./roles'));
router.use('/profiles', require('./profiles'));
router.use('/category', require('./category'));
router.use('/articles', require('./articles'));
router.use('/gallery', require('./gallery'));
router.use('/berita-pemko', require('./news'));
router.use('/opd', require('./dinas'));
router.use('/album', require('./album'));
router.use('/pengumuman', require('./pengumuman'));
router.use('/informasi-dinas', require('./infoDinas'));
router.use('/file', require('./file'));
router.use('/search', require('./search'));

router.get('/', function(req, res){
  return res.status(422).json({success: true, message: "Please refer to the Documentation provided for further information"})
});

router.use(function(err, req, res, next){
  if(err.name === 'ValidationError'){
    return res.status(422).json({
      errors: Object.keys(err.errors).reduce(function(errors, key){
        errors[key] = err.errors[key].message;

        return errors;
      }, {})
    });
  }

  return next(err);
});

Role.roleSeed();

module.exports = router;