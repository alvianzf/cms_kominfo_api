var mongoose = require('mongoose');
var router = require('express').Router();
var passport = require('passport');
var User = mongoose.model('User');
var auth = require('../auth');
var request = require('request');


const upload = require('../../config/uploads').uploads('user-img');
const url = "http://tanjungpinangkota.go.id/api/peg";

router.get('/users', auth.required, function(req, res, next){
  User.find().then(function(user){
    return res.json({success: true, data: user});
  }).catch(next);
});

router.get('/user', auth.required, function(req, res, next){
  User.findById(req.payload.id).then(function(user){
    if(!user){ return res.status(401).json({success:false, message:"Unauthorized"}); }

    return res.json({success:true, data: user.toAuthJSON()});
  }).catch(next);
});

router.put('/user', [ auth.required, upload.single('avatar') ], function(req, res, next){
  User.findById(req.payload.id).then(function(user){
    if(!user){ return res.sendStatus(401); }

    // only update fields that were actually passed...
    if(typeof req.body.username !== 'undefined'){
      user.username = req.body.username;
    }
    if(typeof req.body.email !== 'undefined'){
      user.email = req.body.email;
    }
    if(typeof req.body.role !== 'undefined'){
      user.bio = req.body.role;
    }
    if(typeof req.body.image !== 'undefined'){
      user.image = req.body.image;
    }
    if(typeof req.body.idPeg !== 'undefined'){
      user.idPeg = req.body.idPeg;
    }
    if(typeof req.body.password !== 'undefined'){
      user.setPassword(req.body.password);
    }
    

    return user.save().then(function(){
      return res.json({success:true, data: user.toAuthJSON()});
    });
  }).catch(next);
});

router.post('/users/login', function(req, res, next){
  console.log(req.body.idpeg);
  if(!req.body.idpeg){
    return res.status(422).json({errors: {idpeg: "can't be blank"}});
  }

  if(!req.body.password){
    return res.status(422).json({errors: {password: "can't be blank"}});
  }
  console.log(req.idpeg);
  passport.authenticate('local', {session: false}, function(err, user, info){
    if(err){ return next(err); }

    if(user){
      user.token = user.generateJWT();
      return res.json({success:true, data: user.toAuthJSON()});
    } else {
      return res.status(422).json(info);
    }
  })(req, res, next);
});

router.post('/users', upload.single('avatar'), function(req, res, next){
  var user = new User();
  console.log(user);
  if(req.file){
    user.image = req.file.path;
  }
  
  user.email = req.body.email;
  user.name = req.body.name;
  user.idPeg = req.body.idPeg;
  user.role = req.body.role ? req.body.role : 99;
  user.setPassword(req.body.password);
  console.log(req.body.idPeg);
  user.save().then(function(){
    return res.json({success:true, data: user.toAuthJSON()});
  }).catch(next);
});

router.put('/users/:id', auth.required, function(req, res, next){
  User.findById(req.params.id).then(function(user){
    if(!user){ return res.sendStatus(401); }

    // only update fields that were actually passed...
    if(typeof req.body.email !== 'undefined'){
      user.email = req.body.email;
    }
    if(typeof req.body.name !== 'undefined'){
      user.name = req.body.name;
    }
    if(typeof req.body.role !== 'undefined'){
      user.setPassword(req.body.role);
    }
    if(typeof req.file !== 'undefined'){
      user.image = req.file.path
    }
    if(typeof req.body.password !== 'undefined'){
      user.setPassword(req.body.password);
    }
    
    return user.save().then(function(){
      return res.json({success:true, data:user.toAuthJSON()});
    });
  }).catch(next);
});

// router.get('/pegawai/', function(req, res, next){
//   request({
//       url: url,
//       json: true
//   }, function (error, response, body) {
//       if (!error && response.statusCode === 200) {
//           return res.status(200).json({success:true, data:body.pegawai});
//       }
//   })
// });

router.get('/pegawai/', function(req, res, next){
  request({
      url: url,
      json: true
  }, function (error, response, body) {
    var peg = [];

    for (var i = 0; i < body.pegawai.length; i++){
      var id_peg = body.pegawai[i].id_peg
         nama_gelar = body.pegawai[i].nama_gelar
          link_opd = body.pegawai[i].link_opd
          nama_opd = body.pegawai[i].nama_opd

      var putData = {id_peg: id_peg, nama_gelar: nama_gelar, nama_opd: nama_opd, link_opd: link_opd}
      

      peg.push(putData);
    }
      if (!error && response.statusCode === 200) {
          return res.status(200).json({success:true, data:peg});
      }
  })
});

router.get('/pegawai/:id', function(req, res, next){
  request({
      url: url+"/"+req.params.id,
      json: true
  }, function (error, response, body) {
    var peg = [];

    for (var i = 0; i < body.pegawai.length; i++){
      var id_peg = body.pegawai[i].id_peg
          nama_gelar = body.pegawai[i].nama_gelar
          link_opd = body.pegawai[i].link_opd
          nama_opd = body.pegawai[i].nama_opd

      var putData = {id_peg: id_peg, nama_gelar: nama_gelar, nama_opd: nama_opd, link_opd: link_opd}
      

      peg.push(putData);
    }
      if (!error && response.statusCode === 200) {
          return res.status(200).json({success:true, data:peg});
      }
  })
});
module.exports = router;
