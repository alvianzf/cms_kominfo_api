var router = require('express').Router();
var mongoose = require('mongoose');
var Album = mongoose.model('Album');
var Comment = mongoose.model('Comment');
var User = mongoose.model('User');
var auth = require('../auth');

// Preloads album objects on routes with ':album'
router.param('album', function(req, res, next, slug) {
  Album.findOne({ slug: slug})
    .populate('author')
    .then(function (album) {
      if (!album) { return res.status(404).json({success:false, message:'Not Found'}); }

      req.album = album;

      return next();
    }).catch(next);
});

router.param('comment', function(req, res, next, id) {
  Comment.findById(id).then(function(comment){
    if(!comment) { return res.status(404).json({success:false, message:'Not Found'}); }

    req.comment = comment;

    return next();
  }).catch(next);
});

router.get('/', auth.optional, function(req, res, next) {
  var query = {};
  var limit = 20;
  var offset = 0;

  if(typeof req.query.limit !== 'undefined'){
    limit = req.query.limit;
  }

  if(typeof req.query.offset !== 'undefined'){
    offset = req.query.offset;
  }

  if( typeof req.query.category !== 'undefined' ){
    query.category = {"$in" : [req.query.category]};
  }

  Promise.all([
    req.query.author ? User.findOne({username: req.query.author}) : null,
    req.query.favorited ? User.findOne({username: req.query.favorited}) : null
  ]).then(function(results){
    var author = results[0];
    var favoriter = results[1];

    if(author){
      query.author = author._id;
    }

    if(favoriter){
      query._id = {$in: favoriter.favorites};
    } else if(req.query.favorited){
      query._id = {$in: []};
    }

    return Promise.all([
    Album.find(query)
        .limit(Number(limit))
        .skip(Number(offset))
        .sort({createdAt: 'desc'})
        .populate('author')
        .populate('category')
        .populate('gallery')
        .exec(),
    Album.count(query).exec(),
      req.payload ? User.findById(req.payload.id) : null,
    ]).then(function(results){
      var albums = results[0];
      var albumsCount = results[1];
      var user = results[2];
      return res.json({
        success: true,
        data: albums.map(function(album){
          return album.toJSONFor(user);
        }),
        count: albumsCount
      });
    });
  }).catch(next);
});

router.post('/', auth.required, function(req, res, next) {
  User.findById(req.payload.id).then(function(user){
    if (!user) { return res.sendStatus(401); }

    var album = new Album(req.body);
    album.author = user;
    
    return album.save().then(function(){
      return res.json({success:true, data: album.toJSONFor(user)});
    });
  }).catch(next);
});

// return a album
router.get('/:album', auth.optional, function(req, res, next) {
  Promise.all([
    req.payload ? User.findById(req.payload.id) : null,
    req.album.populate('author').execPopulate(),
    req.album.populate('gallery').execPopulate()
  ]).then(function(results){
    var user = results[0];
    req.album.updateViewCount().then(function(album){
      return res.json({success:true, data: album.toJSONFor(user)});
    });
  }).catch(next);
});

// update album
router.put('/:album', auth.required, function(req, res, next) {
  User.findById(req.payload.id).then(function(user){
    if(req.album.author._id.toString() === req.payload.id.toString()){
      var images = [];
      if(typeof req.body.title !== 'undefined'){
        req.album.title = req.body.title;
      }

      if(typeof req.body.description !== 'undefined'){
        req.album.description = req.body.description;
      }

      if(typeof req.body.category !== 'undefined'){
        req.album.category = req.body.category
      }

      req.album.save().then(function(album){
        return res.json({success:true, data: album.toJSONFor(user)});
      }).catch(next);
    } else {
      return res.status(403).json({success:true, message:"Forbidden"});
    }
  });
});

// delete album
router.delete('/:album', auth.required, function(req, res, next) {
  User.findById(req.payload.id).then(function(user){
    if (!user) { return res.sendStatus(401); }
    return req.album.remove().then(function(){
      return res.status(204).json({success:true, message:"album deleted"});
    }).catch(next);
  }).catch(next);
});

// Favorite an album
router.post('/:album/favorite', auth.required, function(req, res, next) {
  var albumId = req.album._id;
  User.findById(req.payload.id).then(function(user){
    if (!user) { return res.sendStatus(401); }

    return user.favorite(albumId).then(function(){
      return req.album.updateFavoriteCount().then(function(album){
        return res.json({success:true, data: album.toJSONFor(user), message:"Success Favorited"});
      });
    });
  }).catch(next);
});

// Unfavorite an album
router.delete('/:album/favorite', auth.required, function(req, res, next) {
  var albumId = req.album._id;
  
  User.findById(req.payload.id).then(function (user){
    if (!user) { return res.sendStatus(401); }

    return user.unfavorite(albumId).then(function(){
      return req.album.updateFavoriteCount().then(function(album){
        return res.json({success:true, data: album.toJSONFor(user), message:"Success Unfavorited"});
      });
    });
  }).catch(next);
});

// return an gallery's comments
router.get('/:album/comments', auth.optional, function(req, res, next){
  Promise.resolve(req.payload ? User.findById(req.payload.id) : null).then(function(user){
    return req.album.populate({
      path: 'comments',
      populate: {
        path: 'author'
      },
      options: {
        sort: {
          createdAt: 'desc'
        }
      }
    }).execPopulate().then(function(album) {
      return res.json({success:true, data: req.album.comments.map(function(comment){
        return comment.toJSONFor(user);
      })});
    });
  }).catch(next);
});

// create a new comment
router.post('/:album/comments', auth.required, function(req, res, next) {
  User.findById(req.payload.id).then(function(user){
    if(!user){ return res.sendStatus(401); }

    var comment = new Comment(req.body);
    comment.album = req.album;
    comment.author = user;

    return comment.save().then(function(){
      req.album.comments.push(comment);

      return req.album.save().then(function(album) {
        res.json({success:true, data: comment.toJSONFor(user)});
      });
    });
  }).catch(next);
});

router.delete('/:album/comments/:comment', auth.required, function(req, res, next) {
  if(req.comment.author.toString() === req.payload.id.toString()){
    req.album.comments.remove(req.comment._id);
    req.album.save()
      .then(Comment.find({_id: req.comment._id}).remove().exec())
      .then(function(){
        res.status(204).json({success:true, message:"Comment Deleted"})
      });
  } else {
    res.status(403).json({success:false, message:"Forbidden"});
  }
});

module.exports = router;
