var router = require('express').Router();
var mongoose = require('mongoose');
var InfoDinas = mongoose.model('InfoDinas');
var auth = require('../auth');
var User = mongoose.model('User');

// return a list of categories
router.get('/', function(req, res, next) {
  InfoDinas.find().then(function(info){
    return res.json({success: true, data: info});
  }).catch(next);
});

//Category by id
router.get('/:id', function(req, res){
  InfoDinas.findById(req.params.Id).then(function(info){
    return res.json({success: true, data: info})
  })
});

// Create a new category
router.post('/', auth.required, function(req, res, next) {
  User.findById(req.payload.id).then(function(user){
    if (!user) { return res.sendStatus(401); }

    var info = new InfoDinas(req.body);

    info.author = user;

    return info.save().then(function(){
      return res.json({success: true, message: "Berhasil menyimpan Informasi"});
    });
  }).catch(next);
});

// update category
router.put('/:id', auth.required, function(req, res, next) {
  User.findById(req.payload.id).then(function(user){
      let updInfo = {};
      if(typeof req.body.name !== 'undefined'){
        updInfo.name = req.body.name;
      }
      if(typeof req.body.visimisi !== 'undefined'){
        updInfo.name = req.body.visimisi;
      }
      if(typeof req.body.akronim !== 'undefined'){
        updInfo.name = req.body.akronim;
      }
      if(typeof req.body.alamat !== 'undefined'){
        updInfo.name = req.body.alamat;
      }
      if(typeof req.body.tugasfungsi !== 'undefined'){
        updInfo.name = req.body.tugasfungsi;
      }
      if(typeof req.body.kepalakantor !== 'undefined'){
        updInfo.name = req.body.kepalakantor;
      }
      if(typeof req.body.profilkepalakantor !== 'undefined'){
        updInfo.name = req.body.profilkepalakantor;
      }
      updInfo.updatedBy = user;
      
      InfoDinas.findByIdAndUpdate(
        req.params.id,
        updInfo,
        {new:true}
        ).exec().then(function(info){
          if (!info) return res.status(404).json({success:false, message:'Category Not Found'});
          return res.json({success:true, message: "Berhasil mengupdate Informasi"});
      })
  });
});

// delete article
router.delete('/:id', auth.required, function(req, res, next) {
  User.findById(req.payload.id).then(function(user){
    if (!user) { return res.sendStatus(401); }

    InfoDinas.findByIdAndRemove(req.params.id).exec().then(function(info){
      return res.status(204).json({success:true, message: "Behasil menghapus informasi"});
    })
  }).catch(next);
});

module.exports = router;
