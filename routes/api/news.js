// import request from "request";
var router = require('express').Router();
var request = require('request');

const url = "http://tanjungpinangkota.go.id/api/berita_populer";

router.get('/', function(req, res){
    // res.setHeader('Content-Type', 'application/json');
    request({
        url: url,
        json: true
    }, function(error, response, body){
        var beritaPopuler = []
            set = body.berita

        for (var i = 0; i < set.length; i++){
            var id_berita = set[i].id_berita
                judul_berita = set[i].judul_berita
                link_berita = set[i].link_berita
                link_gambar_small = set[i].link_gambar_small ;
                terbit = set[i].tgl_terbit
                tanggal_post = set[i].tgl_post
                tahun_terbit = set[i].tgl_thn_terbit
                bulan_terbit = set[i].tgl_bln_terbit
                tanggal_terbit = set[i].tgl_tgl_terbit
                nama_opd = set[i].nama_opd
                link_opd = set[i].link_opd

            var putData = {id_berita: id_berita, judul_berita: judul_berita, link_berita: link_berita, link_gambar_small: link_gambar_small, terbit: terbit, tanggal_post: tanggal_post, tahun_terbit: tahun_terbit, bulan_terbit: bulan_terbit, tanggal_terbit: tanggal_terbit, nama_opd: nama_opd, link_opd: link_opd}

            beritaPopuler.push(putData)
        }
        if(!error && response.statusCode === 200){
            return res.status(200).json({success:true, data:beritaPopuler});
        }
    })
});

router.get('/all', function(req, res){
    request({
        url: "http://tanjungpinangkota.go.id/api/berita/",
        json: true
    }, function(error, response, body){

        if(!error && response.statusCode === 200){
            var beritaPemko = []
                set = body.berita
            for (var i = 0; i < set.length; i++){
                var putData= {

                    id_berita : set[i].id_berita,
                    judul_berita : set[i].judul_berita,
                    isi_berita_ringkas :set[i].isi_berita_ringkas,
                    isi_berita: set[i].isi_berita,
                    link_berita : set[i].link_berita,
                    link_gambar_small : set[i].link_gambar_small,
                    author:{
                    nip: set[i].nip,
                    nama_penulis : set[i].nama_penulis,
                    foto_peg : set[i].foto_peg,
                    hakakses: set[i].hakakses
                    },
                    kategori: {
                      id_kategori : set[i].id_kategori,
                      kategori : set[i].kategori,
                      link_kategori : set[i].link_kategori
                    },
                    terbit : set[i].tgl_terbit,
                    tanggal_post : set[i].tgl_post,
                    tahun_terbit : set[i].tgl_thn_terbit,
                    bulan_terbit : set[i].tgl_bln_terbit,
                    tanggal_terbit : set[i].tgl_tgl_terbit,
                    tanggal_edit : set[i].tgl_edit,
                    nama_opd : set[i].nama_opd,
                    link_opd :set[i].link_opd
                  }
                beritaPemko.push(putData)
            }
            return res.status(200).json({success:true, data:beritaPemko})
        }
    })
});

router.get('/:slug', function(req, res){
    request({
        url: "http://tanjungpinangkota.go.id/api/berita/"+req.params.slug,
        json: true
    }, function(error, response, body){

        if(!error && response.statusCode === 200){

          var i = 0,
              set = body.berita

              beritaPemko =  {

                  id_berita : set[i].id_berita,
                  judul_berita : set[i].judul_berita,
                  isi_berita_ringkas :set[i].isi_berita_ringkas,
                  isi_berita: set[i].isi_berita,
                  link_berita : set[i].link_berita,
                  link_gambar_small : set[i].link_gambar_small,
                  author:{
                  nip: set[i].nip,
                  nama_penulis : set[i].nama_penulis,
                  foto_peg : set[i].foto_peg,
                  hakakses: set[i].hakakses
                  },
                  kategori: {
                    id_kategori : set[i].id_kategori,
                    kategori : set[i].kategori,
                    link_kategori : set[i].link_kategori
                  },
                  terbit : set[i].tgl_terbit,
                  tanggal_post : set[i].tgl_post,
                  tahun_terbit : set[i].tgl_thn_terbit,
                  bulan_terbit : set[i].tgl_bln_terbit,
                  tanggal_terbit : set[i].tgl_tgl_terbit,
                  tanggal_edit : set[i].tgl_edit,
                  nama_opd : set[i].nama_opd,
                  link_opd :set[i].link_opd
                };

            return res.status(200).json({success: true, data: beritaPemko});
        }
    })
});

module.exports = router;
// export default router;
