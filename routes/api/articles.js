var router = require('express').Router();
var mongoose = require('mongoose');
var Article = mongoose.model('Article');
var Category = mongoose.model('Category');
var Comment = mongoose.model('Comment');
var User = mongoose.model('User');
var auth = require('../auth');

const upload = require('../../config/uploads').uploads('gallery');

// Preloads article objects on routes with ':article'
router.param('article', function(req, res, next, slug) {
  Article.findOne({ slug: slug})
    .populate('author')
    .then(function (article) {
      if (!article) { return res.status(404).json({success:false, message:'Not Found', }); }

      req.article = article;

      return next();
    }).catch(next);
});

router.param('comment', function(req, res, next, id) {
  Comment.findById(id).then(function(comment){
    if(!comment) { return res.status(404).json({success:false, message:'Not Found'}); }

    req.comment = comment;

    return next();
  }).catch(next);
});

router.get('/', auth.optional, function(req, res, next) {
  var query = {};
  var limit = 20;
  var offset = 0;
  console.log(req.params.opd);
  if(typeof req.query.limit !== 'undefined'){
    limit = req.query.limit;
  }

  if(typeof req.query.offset !== 'undefined'){
    offset = req.query.offset;
  }

  if( typeof req.query.category !== 'undefined' ){
    query.category = {"$in" : [req.query.category]};
  }

  Promise.all([
    req.query.author ? User.findOne({username: req.query.author}) : null,
    req.query.favorited ? User.findOne({username: req.query.favorited}) : null
  ]).then(function(results){
    var author = results[0];
    var favoriter = results[1];

    if(author){
      query.author = author._id;
    }

    if(favoriter){
      query._id = {$in: favoriter.favorites};
    } else if(req.query.favorited){
      query._id = {$in: []};
    }

    return Promise.all([
      Article.find(query)
        .limit(Number(limit))
        .skip(Number(offset))
        .sort({createdAt: 'desc'})
        .populate('author')
        .populate('category')
        .exec(),
      Article.count(query).exec(),
      req.payload ? User.findById(req.payload.id) : null,
    ]).then(function(results){
      var articles = results[0];
      var articlesCount = results[1];
      var user = results[2];

      return res.json({
        success: true,
        data: articles.map(function(article){
          // return article.toJSONFor(user);
          return article;

        }),
        count: articlesCount
      });
    });
  }).catch(next);
});

router.get('/opd/:opd', auth.optional, function(req, res, next) {
  var query = {};
  var limit = 20;
  var offset = 0;
  if(typeof req.query.limit !== 'undefined'){
    limit = req.query.limit;
  }

  if(typeof req.query.offset !== 'undefined'){
    offset = req.query.offset;
  }

  if( typeof req.params.opd !== 'undefined' ){
    query.opd = req.params.opd;
  }
  
  Promise.all([
    Article.find(query)
      .limit(Number(limit))
      .skip(Number(offset))
      .sort({createdAt: 'desc'})
      .populate('author')
      .populate('category')
      .exec(),
    Article.count(query).exec(),
    req.payload ? User.findById(req.payload.id) : null,
  ]).then(function(results){
    var articles = results[0];
    var articlesCount = results[1];

    return res.json({
      success: true,
      data: articles.map(function(article){
        // return article.toJSONFor(user);
        return article;

      }),
      count: articlesCount
    });
  }).catch(next);
});

router.get('/category/:category', auth.optional, function(req, res, next) {
  var query = {};
  var limit = 20;
  var offset = 0;
  var category = "";
  if(typeof req.query.limit !== 'undefined'){
    limit = req.query.limit;
  }

  if(typeof req.query.offset !== 'undefined'){
    offset = req.query.offset;
  }

  if( typeof req.params.category !== 'undefined' ){
    Category.findOne({name:req.params.category},{'_id':1})
    .exec().then(function(result){
      query.category = result._id;
    })
  }
  
  Promise.all([
    Article.find(query)
      .limit(Number(limit))
      .skip(Number(offset))
      .sort({createdAt: 'desc'})
      .populate('author')
      .populate('category')
      .exec(),
    Article.count(query).exec(),
    req.payload ? User.findById(req.payload.id) : null,
  ]).then(function(results){
    var articles = results[0];
    var articlesCount = results[1];

    return res.json({
      success: true,
      data: articles.map(function(article){
        // return article.toJSONFor(user);
        return article;

      }),
      count: articlesCount
    });
  }).catch(next);
});

router.get('/feed', auth.required, function(req, res, next) {
  var limit = 20;
  var offset = 0;

  if(typeof req.query.limit !== 'undefined'){
    limit = req.query.limit;
  }

  if(typeof req.query.offset !== 'undefined'){
    offset = req.query.offset;
  }

  User.findById(req.payload.id).then(function(user){
    if (!user) { return res.sendStatus(401); }

    Promise.all([
      Article.find({ author: {$in: user.following}})
        .limit(Number(limit))
        .skip(Number(offset))
        .populate('author')
        .populate('category')
        .exec(),
      Article.count({ author: {$in: user.following}})
    ]).then(function(results){
      var articles = results[0];
      var articlesCount = results[1];
      console.log(user);

      return res.json({
        success: true,
        data: articles.map(function(article){
          return article.toJSONFor(user);
        }),
        count: articlesCount
      });
    }).catch(next);
  });
});

router.post('/', [auth.required, upload.single('thumbnail')], function(req, res, next) {
  User.findById(req.payload.id).then(function(user){
    if (!user) { return res.sendStatus(401); }

    var article = new Article(req.body);

    if(req.file){
      article.thumbnail = req.file.path;
    }

    article.author = user;

    return article.save().then(function(){
      return res.json({success:true, message: "artikel berhasil disimpan"});
    });
  }).catch(next);
});

// return a article
router.get('/:article', auth.optional, function(req, res, next) {
  Promise.all([
    req.payload ? User.findById(req.payload.id) : null,
    req.article.populate('author').execPopulate()
  ]).then(function(results){
    var user = results[0];
    req.article.updateViewCount().then(function(article){
      return res.json({success:true, data: article});
    });
  }).catch(next);
});

// update article
router.put('/:article', [auth.required, upload.single('thumbnail')], function(req, res, next) {
  User.findById(req.payload.id).then(function(user){
    if(req.article.author._id.toString() === req.payload.id.toString()){
      if(typeof req.body.title !== 'undefined'){
        req.article.title = req.body.title;
      }

      if(typeof req.body.description !== 'undefined'){
        req.article.description = req.body.description;
      }

      if(typeof req.file !== 'undefined'){
        req.article.thumbnail = req.file.path;
      }

      if(typeof req.body.body !== 'undefined'){
        req.article.body = req.body.body;
      }

      if(typeof req.body.category !== 'undefined'){
        req.article.category = req.body.category
      }

      req.article.save().then(function(article){
        return res.json({success:true, message: "berhasil mengupdate artikel"});
      }).catch(next);
    } else {
      return res.status(403).json({success:true, message:"Forbidden"});
    }
  });
});

// delete article
router.delete('/:article', auth.required, function(req, res, next) {
  User.findById(req.payload.id).then(function(user){
    console.log(user);
    if (!user) { return res.sendStatus(401); }
    return req.article.remove().then(function(){
      return res.status(204).json({success:true, message:"article deleted"});
    }).catch(next);
  }).catch(next);
});

// Favorite an article
router.post('/:article/favorite', auth.required, function(req, res, next) {
  var articleId = req.article._id;
  User.findById(req.payload.id).then(function(user){
    if (!user) { return res.sendStatus(401); }

    return user.favorite(articleId).then(function(){
      return req.article.updateFavoriteCount().then(function(article){
        return res.json({success:true, message:"Berhasil disimpan"});
      });
    });
  }).catch(next);
});

// Unfavorite an article
router.delete('/:article/favorite', auth.required, function(req, res, next) {
  var articleId = req.article._id;
  
  User.findById(req.payload.id).then(function (user){
    if (!user) { return res.sendStatus(401); }

    return user.unfavorite(articleId).then(function(){
      return req.article.updateFavoriteCount().then(function(article){
        return res.json({success:true, message:"Berhasil dihapus"});
      });
    });
  }).catch(next);
});

// return an article's comments
router.get('/:article/comments', function(req, res, next){
  Promise.resolve(req.payload ? User.findById(req.payload.id) : null).then(function(user){
    return req.article.populate({
      path: 'comments',
      populate: {
        path: 'author'
      },
      options: {
        sort: {
          createdAt: 'desc'
        }
      }
    }).execPopulate().then(function(article) {
      return res.json({success:true, data: req.article.comments.map(function(comment){
        return comment.toJSONFor(user);
      })});
    });
  }).catch(next);
});

// create a new comment
router.post('/:article/comments', auth.required, function(req, res, next) {
  User.findById(req.payload.id).then(function(user){
    if(!user){ return res.sendStatus(401); }

    var comment = new Comment(req.body);
    comment.article = req.article;
    comment.author = user;

    return comment.save().then(function(){
      req.article.comments = req.article.comments.concat([comment]);

      return req.article.save().then(function(article) {
        res.json({success:true, message: "komen Berhasil"});
      });
    });
  }).catch(next);
});

router.delete('/:article/comments/:comment', auth.required, function(req, res, next) {
  if(req.comment.author.toString() === req.payload.id.toString()){
    req.article.comments.remove(req.comment._id);
    req.article.save()
      .then(Comment.find({_id: req.comment._id}).remove().exec())
      .then(function(){
        res.status(204).json({success:true, message:"Comment Deleted"})
      });
  } else {
    res.status(403).json({success:false, message:"Forbidden"});
  }
});

module.exports = router;
