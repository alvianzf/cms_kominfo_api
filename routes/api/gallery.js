var router = require('express').Router();
var mongoose = require('mongoose');
var Gallery = mongoose.model('Gallery');
var Album = mongoose.model('Album');
var User = mongoose.model('User');
var auth = require('../auth');

const upload = require('../../config/uploads').uploads('gallery');

// Preloads gallery objects on routes with ':gallery'
router.param('id', function(req, res, next, id) {
  Gallery.findById(id)
    .populate('author')
    .then(function (gallery) {
      if (!gallery) { return res.status(404).json({success:false, message:'Not Found'}); }

      req.gallery = gallery;

      return next();
    }).catch(next);
});

router.get('/', auth.optional, function(req, res, next) {
  var query = {};
  var limit = 20;
  var offset = 0;

  if(typeof req.query.limit !== 'undefined'){
    limit = req.query.limit;
  }

  if(typeof req.query.offset !== 'undefined'){
    offset = req.query.offset;
  }

  if( typeof req.query.category !== 'undefined' ){
    query.category = {"$in" : [req.query.category]};
  }

  Promise.all([
    req.query.author ? User.findOne({username: req.query.author}) : null,
    req.query.favorited ? User.findOne({username: req.query.favorited}) : null
  ]).then(function(results){
    var author = results[0];
    var favoriter = results[1];

    if(author){
      query.author = author._id;
    }

    if(favoriter){
      query._id = {$in: favoriter.favorites};
    } else if(req.query.favorited){
      query._id = {$in: []};
    }

    return Promise.all([
    Gallery.find(query)
        .limit(Number(limit))
        .skip(Number(offset))
        .sort({createdAt: 'desc'})
        .populate('author')
        .populate('category')
        .exec(),
    Gallery.count(query).exec(),
      req.payload ? User.findById(req.payload.id) : null,
    ]).then(function(results){
      var galleries = results[0];
      var galleriesCount = results[1];
      var user = results[2];

      return res.json({
        success: true,
        data: galleries.map(function(gallery){
          return gallery.toJSONFor(user);
        }),
        count: galleriesCount
      });
    });
  }).catch(next);
});

router.post('/', [auth.required, upload.single('photos')], function(req, res, next) {
  User.findById(req.payload.id).then(function(user){
    if (!user) { return res.sendStatus(401); }

    var gallery = new Gallery(req.body);

    if(req.file){
      gallery.image = req.file.path;
    }
    gallery.author = user;
    
    return gallery.save().then(function(){
      Album.update(
        { _id: req.body.album }, 
        { $push: { gallery: gallery } },
        function(err, result) {
          console.log(err,result)
          return res.json({success:true, data: gallery.toJSONFor(user)});
        }
      )
    });
  }).catch(next);
});

// return a gallery
router.get('/:id', auth.optional, function(req, res, next) {
  Promise.all([
    req.payload ? User.findById(req.payload.id) : null,
    req.gallery.populate('author').execPopulate()
  ]).then(function(results){
    var user = results[0];
    req.gallery.updateViewCount().then(function(gallery){
      return res.json({success:true, data: gallery.toJSONFor(user)});
    });
  }).catch(next);
});

// delete gallery
router.delete('/:gallery', auth.required, function(req, res, next) {
  User.findById(req.payload.id).then(function(user){
    console.log(user);
    if (!user) { return res.sendStatus(401); }
    return req.gallery.remove().then(function(){
      return res.status(204).json({success:true, message:"gallery deleted"});
    }).catch(next);
  }).catch(next);
});


module.exports = router;
