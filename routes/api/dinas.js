
var router = require('express').Router();
var request = require('request');

const url ="http://tanjungpinangkota.go.id/api/unitkerja/";

router.get('/', function(req, res){
    request({
        url: url,
        json: true
    }, function(error, response, body){

        var dinas =[]
            set = body.opd

        for (var i = 0; i < set.length;i++){
            var id_opd = set[i].id_opd
                nama_opd = set[i].nama_opd
                link_opd = set[i].link_opd
                logo = set[i].logo

            var putData = {id_opd: id_opd, nama_opd: nama_opd, link_opd: link_opd, logo: logo}
        dinas.push(putData)
        }

        if(!error && response.statusCode === 200){
            return res.status(200).json({success:true, data:dinas});
        }
    })    
});


// GET single OPD by link_opd
router.get('/:opd', function(req, res){
    request({
        url: "http://tanjungpinangkota.go.id/api/unitkerja/"+ req.params.opd,
        json: true
    }, function(error, response, body){

        var dinas =[]
            set = body.opd

        for (var i = 0; i < set.length;i++){
            var id_opd = set[i].id_opd
                nama_opd = set[i].nama_opd
                link_opd = set[i].link_opd
                logo = set[i].logo

            var putData = {id_opd: id_opd, nama_opd: nama_opd, link_opd: link_opd, logo: logo}
        dinas.push(putData)
        }

        if(!error && response.statusCode === 200){
            return res.status(200).json({success:true, data:dinas});
        }
    })
});

module.exports = router;