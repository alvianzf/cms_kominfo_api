var router = require('express').Router();
var mongoose = require('mongoose');
var File = mongoose.model('File');
var User = mongoose.model('User');
var auth = require('../auth');

const upload = require('../../config/uploads').uploads('file');

// Preloads gallery objects on routes with ':gallery'
router.param('slug', function(req, res, next, slug) {
  File.findOne({slug:slug})
    .populate('author')
    .then(function (file) {
      if (!file) { return res.status(404).json({success:false, message:'Not Found'}); }

      req.file = file;

      return next();
    }).catch(next);
});

router.get('/', auth.optional, function(req, res, next) {
  var query = {};
  var limit = 20;
  var offset = 0;

  if(typeof req.query.limit !== 'undefined'){
    limit = req.query.limit;
  }

  if(typeof req.query.offset !== 'undefined'){
    offset = req.query.offset;
  }

  Promise.all([
    req.query.author ? User.findOne({username: req.query.author}) : null,
  ]).then(function(results){
    var author = results[0];

    if(author){
      query.author = author._id;
    }

    return Promise.all([
    File.find(query)
        .limit(Number(limit))
        .skip(Number(offset))
        .sort({createdAt: 'desc'})
        .populate('author')
        .exec(),
    File.count(query).exec(),
      req.payload ? User.findById(req.payload.id) : null,
    ]).then(function(results){
      var files = results[0];
      var filesCount = results[1];
      var user = results[2];

      return res.json({
        success: true,
        data: files.map(function(file){
          return file.toJSONFor(user);
        }),
        count: filesCount
      });
    });
  }).catch(next);
});

router.post('/', [auth.required, upload.single('file')], function(req, res, next) {
  User.findById(req.payload.id).then(function(user){
    if (!user) { return res.sendStatus(401); }

    var file = new File(req.body);

    if(req.file){
        file.file = req.file.path;
    }
    file.author = user;
    
    return file.save().then(function(){
        return res.json({success:true, data: file.toJSONFor(user)});
    });
  }).catch(next);
});

// return a gallery
router.get('/:slug', auth.optional, function(req, res, next) {
  Promise.all([
    req.payload ? User.findById(req.payload.id) : null,
    req.file.populate('author').execPopulate()
  ]).then(function(results){
    var user = results[0];
    req.file.updateViewCount().then(function(file){
      return res.json({success:true, data: file.toJSONFor(user)});
    });
  }).catch(next);
});

// delete gallery
router.delete('/:file', auth.required, function(req, res, next) {
  User.findById(req.payload.id).then(function(user){
    if (!user) { return res.sendStatus(401); }
    return req.file.remove().then(function(){
      return res.status(204).json({success:true, message:"file deleted"});
    }).catch(next);
  }).catch(next);
});


module.exports = router;
