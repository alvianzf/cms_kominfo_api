module.exports = [
    { 
      _id: 0,
      name: "Superadmin",
      author: null,
      updatedBy: null,
      createdAt: Date.now(),
      updatedAt: Date.now()
    },
    {
      _id: 1,
      name: "Administrator",
      author: null,
      updatedBy: null,
      createdAt: Date.now(),
      updatedAt: Date.now()
    },
    {
      _id: 2,
      name: "Editor",
      author: null,
      updatedBy: null,
      createdAt: Date.now(),
      updatedAt: Date.now()
    },
    {
      _id: 3,
      name: "Pegawai",
      author: null,
      updatedBy: null,
      createdAt: Date.now(),
      updatedAt: Date.now()
    },
    {
      _id:99,
      name: "Pengunjung",
      author: null,
      updatedBy: null,
      createdAt: Date.now(),
      updatedAt: Date.now()
    }
]