var passport = require('passport');
var LocalStrategy = require('passport-local').Strategy;
var mongoose = require('mongoose');
var User = mongoose.model('User');

passport.use(new LocalStrategy({
  usernameField: 'idpeg',
  passwordField: 'password',
}, function(idpeg, password, done) {
  User
  .findOne({ idPeg:idpeg.toString() })
  .populate('role', '_id name')
  .then(function(user){
    if(!user || !user.validPassword(password)){
      return done(null, false, {errors: {'username or password': 'is invalid'}});
    }

    return done(null, user);
  }).catch(done);
}));

