var multer = require('multer');
var crypto = require('crypto');
var mime = require('mime');
var fs = require('fs');

module.exports.uploads = function(path){
    var mdl = {};
    var newPath = path ? 'public/uploads/'+path : 'public/uploads/';
    var storage = multer.diskStorage({
        destination: function (req, file, cb) {
          fs.mkdir(newPath, err => cb(null, newPath))
        },
        filename: function (req, file, cb) {
          console.log(file);
          if (!file.originalname.match(/\.(jpg|jpeg|png|gif)$/)) {
            cb(null, Date.now() + file.originalname);
          }else{
            crypto.pseudoRandomBytes(16, function (err, raw) {
              cb(null, raw.toString('hex') + Date.now() + '.' + mime.getExtension(file.mimetype));
            });
          }
        }
    });
    
    return multer({ storage: storage });
}