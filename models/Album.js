var mongoose = require('mongoose');
var uniqueValidator = require('mongoose-unique-validator');
var slug = require('slug');
var User = mongoose.model('User');

var AlbumSchema = new mongoose.Schema({
  slug: { type: String, lowercase: true, unique: true },
  title: String,
  description: String,
  opd: String,
  favoritesCount: { type: Number, default: 0 },
  viewsCount: { type: Number, default: 0 },
  status: { type: String, default: 'Draft' },
  allowComment: { type: Boolean, default: true },
  gallery: [{ type: mongoose.Schema.Types.ObjectId, ref: 'Gallery' }],
  comments: [{ type: mongoose.Schema.Types.ObjectId, ref: 'Comment' }],
  category: [{ type: mongoose.Schema.Types.ObjectId, ref: 'Category' }],
  author: { type: mongoose.Schema.Types.ObjectId, ref: 'User' },
  updatedBy: { type: mongoose.Schema.Types.ObjectId, ref: 'User', default:null },
  publishedBy: { type: mongoose.Schema.Types.ObjectId, ref: 'User', default:null },
  publishedAt: { type: Date, default:null },
}, {timestamps: true});

AlbumSchema.plugin(uniqueValidator, {message: 'is already taken'});

AlbumSchema.pre('validate', function(next){
  if(!this.slug)  {
    this.slugify();
  }

  next();
});

AlbumSchema.methods.slugify = function() {
  this.slug = slug(this.title) + '-' + (Math.random() * Math.pow(36, 6) | 0).toString(36);
};

AlbumSchema.methods.updateFavoriteCount = function() {
  var album = this;

  return User.count({favorites: {$in: [album._id]}}).then(function(count){
    album.favoritesCount = count;

    return album.save();
  });
};

AlbumSchema.methods.updateViewCount = function() {
  var album = this;
  album.viewsCount++;
  return album.save();
};
// Requires population of author
AlbumSchema.methods.toJSONFor = function(user){
  return {
    _id: this._id,
    slug: this.slug,
    title: this.title,
    opd: this.opd,
    gallery: this.gallery,
    description: this.description,
    createdAt: this.createdAt,
    updatedAt: this.updatedAt,
    category: this.category,
    status: this.status,
    favorited: user ? user.isFavorite(this._id) : false,
    favoritesCount: this.favoritesCount,
    viewsCount: this.favoritesCount,
    author: this.author.toProfileJSONFor(user),
    updatedBy: this.updatedBy ? this.updatedBy.toProfileJSONFor(user) : null, 
    publishedBy: this.publishedBy ? this.publishedBy.toProfileJSONFor(user) : null,
  };
};

AlbumSchema.index({
  title: 'text',
  description: 'text',
}, {
  weights: {
    name: 5,
    description: 1,
  },
});

mongoose.model('Album', AlbumSchema);
