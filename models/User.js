var mongoose = require('mongoose');
var uniqueValidator = require('mongoose-unique-validator');
var crypto = require('crypto');
var jwt = require('jsonwebtoken');
var secret = require('../config').secret;

var UserSchema = new mongoose.Schema({
  email: {type: String, lowercase: true, unique: true, required: [false], match: [/\S+@\S+\.\S+/, 'is invalid'], index: true, default:null},
  image: String,
  name: { type: String, required: [true, "can't be blank"] },
  idPeg: {type: String, default:0 },
  opd: {type: String},
  role: {type: Number, ref:'Role', required: [true, "can't be blank"]},
  favorites: [{ type: mongoose.Schema.Types.ObjectId, ref: 'Article' }],
  hash: String,
  salt: String
}, {timestamps: true});

UserSchema.plugin(uniqueValidator, {message: 'is already taken.'});

UserSchema.methods.validPassword = function(password) {
  var hash = crypto.pbkdf2Sync(password, this.salt, 10000, 512, 'sha512').toString('hex');
  return this.hash === hash;
};

UserSchema.methods.setPassword = function(password){
  this.salt = crypto.randomBytes(16).toString('hex');
  this.hash = crypto.pbkdf2Sync(password, this.salt, 10000, 512, 'sha512').toString('hex');
};

UserSchema.methods.generateJWT = function() {
  var today = new Date();
  var exp = new Date(today);
  exp.setDate(today.getDate() + 60);

  return jwt.sign({
    id: this._id,
    idPeg: this.idPeg,
    image: this.image,
    name: this.name,
    role: this.role,
    exp: parseInt(exp.getTime() / 1000),
  }, secret);
};

UserSchema.methods.toAuthJSON = function(){
  return {
    name: this.name,
    email: this.email ? this.email : null,
    idPeg: this.idPeg ? this.idPeg : null,
    role: this.role,
    token: this.generateJWT(),
    image: this.image
  };
};

UserSchema.methods.toProfileJSONFor = function(user){
  return {
    idPeg: this.idPeg,
    name: this.name,
    role: this.role,
    image: this.image || 'https://ui-avatars.com/api/?name=' + this.name,
  };
};

UserSchema.methods.favorite = function(id){
  if(this.favorites.indexOf(id) === -1){
    this.favorites.push(id);
  }

  return this.save();
};

UserSchema.methods.unfavorite = function(id){
  this.favorites.remove(id);
  return this.save();
};

UserSchema.methods.isFavorite = function(id){
  return this.favorites.some(function(favoriteId){
    return favoriteId.toString() === id.toString();
  });
};

mongoose.model('User', UserSchema);
