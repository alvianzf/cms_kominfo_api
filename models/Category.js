var mongoose = require('mongoose');

var CategorySchema = new mongoose.Schema({
  name: String,
  author: { type: mongoose.Schema.Types.ObjectId, ref: 'User' },
  updatedBy: { type: mongoose.Schema.Types.ObjectId, ref: 'User' },
}, {timestamps: true});

// Requires population of author
CategorySchema.methods.toJSONFor = function(user){
  return {
    id: this._id,
    name: this.name,
    createdAt: this.createdAt,
    updatedBy: this.updatedBy ? this.updatedBy.toProfileJSONFor(user) : null,
    author: this.author.toProfileJSONFor(user)
  };
};

mongoose.model('Category', CategorySchema);
