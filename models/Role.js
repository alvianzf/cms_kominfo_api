var mongoose = require('mongoose');
var seeder = require('mongoose-seeder');
var data = require('../seed/roles');

const RoleSchema = new mongoose.Schema({
  _id : { type: Number, unique: true, required: true},
  name: { type: String, unique: true, required: true },
  author: { type: mongoose.Schema.Types.ObjectId, ref: 'User' },
  updatedBy: { type: mongoose.Schema.Types.ObjectId, ref: 'User' },
}, { timestamps: true });

RoleSchema.statics.roleSeed = function(){
  let roles = this;
  
  roles.find({
    '_id':0
  }).then(result => {
    if(!result.length > 0){
      roles.insertMany(data).then(docs => {
        console.log(docs)
      }).catch(err => {
        console.log(err)
      })
    }
  })
}
// Requires population of author
RoleSchema.methods.toJSONFor = function(user){
  return {
    name: this.name,
    createdAt: this.createdAt,
    updatedBy: this.updatedBy ? this.updatedBy.toProfileJSONFor(user) : null,
    author: this.author.toProfileJSONFor(user)
  };
};

mongoose.model('Role', RoleSchema);
