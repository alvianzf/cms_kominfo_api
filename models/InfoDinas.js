var mongoose = require('mongoose');

var InfoDinasSchema = new mongoose.Schema({
  name: String,
  akronim: String,
  visimisi: String,
  alamat: String,
  tugasfungsi: String,
  kepalakantor: String,
  profilkepalakantor: String,
  author: { type: mongoose.Schema.Types.ObjectId, ref: 'User' },
  updatedBy: { type: mongoose.Schema.Types.ObjectId, ref: 'User' },
}, {timestamps: true});

// Requires population of author
InfoDinasSchema.methods.toJSONFor = function(user){
  return {
    id: this._id,
    name: this.name,
    akronim: this.akronim,
    visimisi: this.visimisi,
    alamat: this.alamat,
    tugasfungsi: this.tugasfungsi,
    kepalakantor: this.kepalakantor,
    profilkepalakantor: this.profilkepalakantor,
    createdAt: this.createdAt,
    updatedAt: this.updatedAt,
    updatedBy: this.updatedBy ? this.updatedBy.toProfileJSONFor(user) : null,
    author: this.author.toProfileJSONFor(user)
  };
};

InfoDinasSchema.index({
  name: 'text',
}, {
  weights: {
    name: 5,
  },
});

mongoose.model('InfoDinas', InfoDinasSchema);
