var mongoose = require('mongoose');
var uniqueValidator = require('mongoose-unique-validator');
var slug = require('slug');
// var User = mongoose.model('User');

var FileSchema = new mongoose.Schema({
  slug: { type: String, lowercase: true, unique: true },
  title: String,
  description: String,
  opd: String,
  file: { type:String, required: [true, "can't be blank"] },
  global: Boolean,
  viewsCount: { type: Number, default: 0 },
  status: { type: String, default: 'Draft' },
  allowComment: { type: Boolean, default: true },
  author: { type: mongoose.Schema.Types.ObjectId, ref: 'User' },
  updatedBy: { type: mongoose.Schema.Types.ObjectId, ref: 'User', default:null },
  publishedBy: { type: mongoose.Schema.Types.ObjectId, ref: 'User', default:null },
  publishedAt: { type: Date, default:null },
}, {timestamps: true});

FileSchema.plugin(uniqueValidator, {message: 'is already taken'});

FileSchema.pre('validate', function(next){
  if(!this.slug)  {
    this.slugify();
  }

  next();
});

FileSchema.methods.slugify = function() {
  this.slug = slug(this.title) + '-' + (Math.random() * Math.pow(36, 6) | 0).toString(36);
};

FileSchema.methods.updateViewCount = function() {
  var album = this;
  album.viewsCount++;
  return album.save();
};
// Requires population of author
FileSchema.methods.toJSONFor = function(user){
  return {
    _id: this._id,
    slug: this.slug,
    title: this.title,
    opd: this.opd,
    file: this.file,
    global: this.global,
    description: this.description,
    createdAt: this.createdAt,
    updatedAt: this.updatedAt,
    status: this.status,
    viewsCount: this.favoritesCount,
    author: this.author.toProfileJSONFor(user),
    updatedBy: this.updatedBy ? this.updatedBy.toProfileJSONFor(user) : null, 
    publishedBy: this.publishedBy ? this.publishedBy.toProfileJSONFor(user) : null,
  };
};

FileSchema.index({
  title: 'text',
  description: 'text',
  opd: 'text'
}, {
  weights: {
    title: 5,
    description: 2,
    opd: 1
  },
});

mongoose.model('File', FileSchema);
