var mongoose = require('mongoose');
var User = mongoose.model('User');

var GallerySchema = new mongoose.Schema({
  image: String,
  album: { type: mongoose.Schema.Types.ObjectId, ref: 'Album' },
  viewsCount: { type: Number, default: 0 },
  author: { type: mongoose.Schema.Types.ObjectId, ref: 'User' },
  updatedBy: { type: mongoose.Schema.Types.ObjectId, ref: 'User', default:null },
}, {timestamps: true});

GallerySchema.methods.updateViewCount = function() {
  var gallery = this;
  gallery.viewsCount++;
  return gallery.save();
};

GallerySchema.methods.toJSONFor = function(user){
  return {
    _id: this._id,
    image: this.image,
    album: this.album,
    createdAt: this.createdAt,
    updatedAt: this.updatedAt,
    viewsCount: this.viewsCount,
    author: this.author.toProfileJSONFor(user),
    updatedBy: this.updatedBy ? this.updatedBy.toProfileJSONFor(user) : null
  };
};

mongoose.model('Gallery', GallerySchema);
