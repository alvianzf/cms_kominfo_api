var mongoose = require('mongoose');
var uniqueValidator = require('mongoose-unique-validator');
var slug = require('slug');
var User = mongoose.model('User');

var ArticleSchema = new mongoose.Schema({
  slug: { type: String, lowercase: true, unique: true },
  title: String,
  description: String,
  thumbnail: String,
  body: String,
  opd: String,
  favoritesCount: { type: Number, default: 0 },
  viewsCount: { type: Number, default: 0 },
  status: { type: String, default: 'Draft' },
  allowComment: { type: Boolean, default: true },
  comments: [{ type: mongoose.Schema.Types.ObjectId, ref: 'Comment' }],
  category: [{ type: mongoose.Schema.Types.ObjectId, ref: 'Category' }],
  author: { type: mongoose.Schema.Types.ObjectId, ref: 'User' },
  updatedBy: { type: mongoose.Schema.Types.ObjectId, ref: 'User', default: null },
  publishedBy: { type: mongoose.Schema.Types.ObjectId, ref: 'User', default: null },
  publishedAt: { type: Date, default: null },
}, {usePushEach: true, timestamps: true});

ArticleSchema.plugin(uniqueValidator, {message: 'is already taken'});

ArticleSchema.pre('validate', function(next){
  if(!this.slug)  {
    this.slugify();
  }

  next();
});

ArticleSchema.methods.slugify = function() {
  this.slug = slug(this.title) + '-' + (Math.random() * Math.pow(36, 6) | 0).toString(36);
};

ArticleSchema.methods.updateFavoriteCount = function() {
  var article = this;

  return User.count({favorites: {$in: [article._id]}}).then(function(count){
    article.favoritesCount = count;

    return article.save();
  });
};

ArticleSchema.methods.updateViewCount = function() {
  var article = this;

  return User.count({views: {$in: [article._id]}}).then(function(count){
    article.viewsCount = count;

    return article.save();
  });
};

ArticleSchema.methods.toJSONFor = function(user){
  return {
    slug: this.slug,
    title: this.title,
    description: this.description,
    body: this.body,
    opd: this.opd,
    thumbnail: this.thumbnail,
    createdAt: this.createdAt,
    updatedAt: this.updatedAt,
    category: this.category,
    status: this.status,
    favorited: user ? user.isFavorite(this._id) : false,
    favoritesCount: this.favoritesCount,
    viewsCount: this.viewsCount,
    author: this.author.toProfileJSONFor(user),
    updatedBy: this.updatedBy ? this.updatedBy.toProfileJSONFor(user) : null,
    publishedBy: this.updatedBy ? this.publishedBy.toProfileJSONFor(user) : null,
    publishedAt: this.updatedAt ? this.publishedAt : null
  };
};

ArticleSchema.index({
  title: 'text',
  description: 'text',
  body: 'text',
  opd: 'text'
}, {
  weights: {
    title: 5,
    description: 2,
    body: 1,
    opd: 1
  },
});

mongoose.model('Article', ArticleSchema);
