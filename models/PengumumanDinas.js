var mongoose = require('mongoose');
var uniqueValidator = require('mongoose-unique-validator');
var slug = require('slug');
var User = mongoose.model('User');

var PengumumanDinasSchema = new mongoose.Schema({
  slug: { type: String, lowercase: true, unique: true },
  title: String,
  description: String,
  thumbnail: String,
  body: String,
  opd: String,
  global: Boolean,
  favoritesCount: { type: Number, default: 0 },
  viewsCount: { type: Number, default: 0 },
  status: { type: String, default: 'Draft' },
  allowComment: { type: Boolean, default: true },
  comments: [{ type: mongoose.Schema.Types.ObjectId, ref: 'Comment' }],
  author: { type: mongoose.Schema.Types.ObjectId, ref: 'User' },
  updatedBy: { type: mongoose.Schema.Types.ObjectId, ref: 'User', default: null },
  publishedBy: { type: mongoose.Schema.Types.ObjectId, ref: 'User', default: null },
  publishedAt: { type: Date, default: null },
}, {usePushEach: true, timestamps: true});

PengumumanDinasSchema.plugin(uniqueValidator, {message: 'is already taken'});

PengumumanDinasSchema.pre('validate', function(next){
  if(!this.slug)  {
    this.slugify();
  }

  next();
});

PengumumanDinasSchema.methods.slugify = function() {
  this.slug = slug(this.title) + '-' + (Math.random() * Math.pow(36, 6) | 0).toString(36);
};

PengumumanDinasSchema.methods.updateFavoriteCount = function() {
  var pengumuman = this;

  return User.count({favorites: {$in: [pengumuman._id]}}).then(function(count){
    pengumuman.favoritesCount = count;

    return pengumuman.save();
  });
};

PengumumanDinasSchema.methods.updateViewCount = function() {
  var pengumuman = this;

  return User.count({views: {$in: [pengumuman._id]}}).then(function(count){
    pengumuman.viewsCount = count;

    return pengumuman.save();
  });
};

PengumumanDinasSchema.methods.toJSONFor = function(user){
  return {
    slug: this.slug,
    title: this.title,
    description: this.description,
    body: this.body,
    opd: this.opd,
    global: this.global,
    thumbnail: this.thumbnail,
    createdAt: this.createdAt,
    updatedAt: this.updatedAt,
    status: this.status,
    favorited: user ? user.isFavorite(this._id) : false,
    favoritesCount: this.favoritesCount,
    viewsCount: this.viewsCount,
    author: this.author.toProfileJSONFor(user),
    updatedBy: this.updatedBy ? this.updatedBy.toProfileJSONFor(user) : null,
    publishedBy: this.updatedBy ? this.publishedBy.toProfileJSONFor(user) : null,
    publishedAt: this.updatedAt ? this.publishedAt : null
  };
};

PengumumanDinasSchema.index({
  title: 'text',
  description: 'text',
  body: 'text',
  opd: 'text'
}, {
  weights: {
    title: 5,
    description: 2,
    body: 1,
    opd: 1
  },
});

mongoose.model('PengumumanDinas', PengumumanDinasSchema);
