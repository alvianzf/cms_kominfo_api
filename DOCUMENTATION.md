# DOCUMENTATION FOR CMS OPD Pemerintah Kota Tanjungpinang

API endpoint format for the project is: `(base_url)/api/(endpoint_name)/:parameter`

>This project was built using `NodeJs` and `ReactJs` with `MongoDB` as database. Please refer to the NPM guidelines for proper installation and configurations.

## API LIST

### AUTH

#### Register `POST`
endpoint: `(base_url)/api/users`

Headers:
> **Content-Type**  application/x-www-form-url-encoded  
**X-Requested-With**  XMLHttpRequest

Body:
```
```

#### Login `POST`

endpoint : `(base_url)/api/users/login`

Headers:
>**Content-Type** application/json  
**X-requested-with** XMLHttpRequest

Body:
```
{"email":"test@test.com", "password":"test123"}
```

#### Login with idPeg `POST`

endpoint `(base_url)/api/users/login`

Headers:
>**Content-Type** application/json  
**X-requested-with** XMLHttpRequest

Body:
```
{"idpeg":196704161994011001, "password":"test123"}
```

#### Current User `GET`

endppoint `(base_url)/api/user`

Headers:
>**Content-Type** application/json  
**X-requested-with** XMLHttpRequest  
**Authorization** Token{{token}}

Body:
```
```

#### Update Current User `PUT`

endpoint `(base_url)/api/user`

Headers:
>**Content-Type** application/json  
**X-requested-with** XMLHttpRequest  
**Authorization** Token{{token}}

Body:
```
{"email":"test@test.com"}
```

### ROLES

#### All Roles `GET`

endpoint `(base_url)/api/role`

Headers:
>**Content-Type** application/x-www-form-urlencoded  
**X-requested-with** XMLHttpRequest

Body:
```
```

#### Create Roles `POST`

endpoint `(base_url)/api/role`

Headers:
>**Content-Type** application/json
**X-requested-with** XMLHttpRequest  
**Authorization** Token{{token}}

Body:
```
{
    "id": 99,
    "name": "test
}
```

#### Update Roles `PUT`

endpoint `(base_url)/api/role/0`

Headers:
>**Content-Type** application/json  
**X-requested-with** XMLHttpRequest  
**Authorization** Token{{token}}

Body:
```
{
    "name": "Update Role"
}
```

#### Delete Role `DEL`

endpoint `(base_url)/api/role/99`

Headers:
>**Content-Type** application/json  
**X-requested-with** XMLHttpRequest  
**Authorization** Token{{token}}

Body:
```
```

### CATEGORIES

#### All Categories `GET`

endpoint `(base_url)/api/category`

Headers:
>**Content-**Type application/json  
**X-requested-with** XMLHttpRequest  
**Authorization** Token{{token}}

Body:
```

```

Response:

```
[
    {
        "success": true,
        data: [
            {
                "name": "category-name-1"
            },
            {
                "name": "category-name-2"
            }
        ]
    }
]

```

#### Create Category `POST

endpoint `(base_url)/api/category`

Headers:
>**Content-Type** application/json  
**X-requested-with** XMLHttpRequest  
**Authorization** Token{{token}}

Body:
```
{
    "name": "category-name"
}
```

#### Update Category `PUT`

endpoint `(base_url)/api/category/:id`

Headers:
>**Content-Type** application/json  
**X-requested-with** XMLHttpRequest  
**Authorization** Token{{token}}

Body:
```
```

#### Delete Category `DEL`

endpoint `(base_url)/api/category/:id`
Headers:
>**Content-Type** application/json  
**X-requested-with** XMLHttpRequest  
***Authorization** Toen {{token}}

Body:
```
```

###